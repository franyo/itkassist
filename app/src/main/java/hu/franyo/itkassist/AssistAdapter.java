package hu.franyo.itkassist;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AssistAdapter extends ArrayAdapter<AssistItem> {

    private final Context context;
    private ArrayList<AssistItem> itemList;
    private int selectedPosition = -1;

    public AssistAdapter(Context context, ArrayList<AssistItem> itemList) {
        super(context, -1);

        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        TextView questionTV = (TextView) rowView.findViewById(R.id.itemQ);
        TextView answerTV = (TextView) rowView.findViewById(R.id.itemA);

        questionTV.setText(itemList.get(position).getQuestion());
        answerTV.setText(itemList.get(position).getAnswer());

        int color = Color.BLACK;
        if (position == selectedPosition) {
            color = Color.WHITE;
        }
        questionTV.setTextColor(color);
        answerTV.setTextColor(color);

        return rowView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }
}