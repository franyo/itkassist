package hu.franyo.itkassist.answer;

public interface AnswerHandler {

    void onAnswer(String answer);
}