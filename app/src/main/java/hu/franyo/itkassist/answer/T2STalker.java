package hu.franyo.itkassist.answer;

import android.app.Activity;
import android.util.Log;

import org.ispeech.SpeechSynthesis;
import org.ispeech.SpeechSynthesisEvent;
import org.ispeech.error.BusyException;
import org.ispeech.error.InvalidApiKeyException;
import org.ispeech.error.NoNetworkException;

import hu.franyo.itkassist.question.ASRHandler;

public class T2STalker {

    private static final String TAG = "Talker_LOG";

    private SpeechSynthesis synthesis;
    private Activity context;
    private ASRHandler handler;

    public T2STalker(Activity context, ASRHandler handler) {
        this.context = context;
        this.handler = handler;

        prepareTTSEngine();
    }

    private void prepareTTSEngine() {
        try {
            synthesis = SpeechSynthesis.getInstance(context);
            synthesis.setSpeechSynthesisEvent(new SpeechSynthesisEvent() {

                public void onPlaySuccessful() {
                    Log.i(TAG, "onPlaySuccessful");
                }

                public void onPlayStopped() {
                    Log.i(TAG, "onPlayStopped");
                }

                public void onPlayFailed(Exception e) {
                    Log.e(TAG, "onPlayFailed\n" + e.toString() + "\n" + "\n" + e.getStackTrace());
                    handler.onError(e.toString());
                }

                public void onPlayStart() {
                    Log.i(TAG, "onPlayStart");
                }

                @Override
                public void onPlayCanceled() {
                    Log.i(TAG, "onPlayCanceled");
                }

            });
        } catch (InvalidApiKeyException e) {
            Log.e(TAG, "Invalid API key\n" + e.getStackTrace());
            handler.onError("ERROR: Invalid API key");
        }
    }

    public void setVoice(String voice) {
        synthesis.setVoiceType(voice);
    }

    public void talk(String ttsText) {
        try {
            synthesis.speak(ttsText);
        } catch (BusyException e) {
            Log.e(TAG, "SDK is busy");
            e.printStackTrace();
            handler.onError("ERROR: SDK is busy");
        } catch (NoNetworkException e) {
            Log.e(TAG, "Network is not available\n" + e.getStackTrace());
            handler.onError("ERROR: Network is not available");
        }
    }
}