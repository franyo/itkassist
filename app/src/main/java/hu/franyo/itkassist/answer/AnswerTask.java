package hu.franyo.itkassist.answer;

import android.os.AsyncTask;

public class AnswerTask extends AsyncTask<Void, Void, String> {

    private AnswerHandler handler;
    private String url;
    private String postData;

    public AnswerTask(AnswerHandler handler, String url, String postData) {
        this.handler = handler;
        this.url = url;
        this.postData = postData;
    }

    @Override
    protected String doInBackground(Void... params) {
        String answer = new HttpPost(url, postData).performPostCall();
        return answer;
    }

    protected void onPostExecute(String answer) {
        handler.onAnswer(answer);
    }
}
