package hu.franyo.itkassist.question;

import java.util.ArrayList;

public class ASRResult {

    private ArrayList<String> resultList;
    private float[] confidenceArray;

    public ASRResult(ArrayList<String> resultList, float[] confidenceArray) {
        this.resultList = resultList;
        this.confidenceArray = confidenceArray;
    }

    public ASRResult(ArrayList<String> resultList) {
        this.resultList = resultList;
    }

    public ArrayList<String> getResultList() {
        return resultList;
    }

    public void setResultList(ArrayList<String> resultList) {
        this.resultList = resultList;
    }

    public float[] getConfidenceArray() {
        return confidenceArray;
    }

    public void setConfidenceArray(float[] confidenceArray) {
        this.confidenceArray = confidenceArray;
    }
}
