package hu.franyo.itkassist.question.mock;

import android.speech.RecognitionListener;

public interface MockRecognitionListener extends RecognitionListener {

    void onResultMock(String mockResult);
}
