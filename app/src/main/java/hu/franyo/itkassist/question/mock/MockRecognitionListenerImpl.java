package hu.franyo.itkassist.question.mock;

import android.util.Log;

import java.util.ArrayList;

import hu.franyo.itkassist.question.ASRHandler;
import hu.franyo.itkassist.question.RecognitionListenerImpl;
import hu.franyo.itkassist.question.ASRResult;

public class MockRecognitionListenerImpl extends RecognitionListenerImpl implements MockRecognitionListener {

    private static final String LOG_TAG = "MOCK_ListenerImpl_LOG";

    public MockRecognitionListenerImpl(ASRHandler handler) {
        super(handler);
    }

    @Override
    public void onResultMock(String mockResult) {
        Log.i(LOG_TAG, "MOCK: onResults");
        ArrayList<String> matches = new ArrayList<>();
        matches.add(mockResult);
        float[] conf = new float[]{1};
        handler.onResult(new ASRResult(matches, conf), true);
    }
}