package hu.franyo.itkassist.question;

public interface ASRHandler {

    void onEndOfSpeech();
    void onError(String error);
    void onResult(ASRResult result, Boolean play);
    void onPartialResult(ASRResult result);
}
