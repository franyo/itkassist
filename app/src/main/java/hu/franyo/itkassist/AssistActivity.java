package hu.franyo.itkassist;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import hu.franyo.itkassist.answer.AnswerHandler;
import hu.franyo.itkassist.answer.AnswerTask;
import hu.franyo.itkassist.answer.T2STalker;
import hu.franyo.itkassist.question.ASRHandler;
import hu.franyo.itkassist.question.ASRResult;
import hu.franyo.itkassist.question.RecognitionListenerImpl;

public class AssistActivity extends AppCompatActivity {

    private SpeechRecognizer speech;
    private Intent recognizerIntent;
    private T2STalker talker;

    private Boolean yesNoMode = false;
    private Boolean listening = false;

    private TextView partialQuestion;
    private ArrayList<AssistItem> itemList;

    private ArrayList<ArrayList<String>> currentAnswerList = null;
    private String currentAnswer = null;
    private Integer currentAnswerIndex = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        partialQuestion = (TextView) findViewById(R.id.partialQuestion);

        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listening) {
                    speech.stopListening();
                } else {
                    speech.startListening(recognizerIntent);
                }
            }
        });

        itemList = new ArrayList<>();
        final AssistAdapter adapter = new AssistAdapter(this, itemList);
        final ListView listView = (ListView) findViewById(R.id.assistList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                currentAnswer = null;
                ArrayList<String> list = itemList.get(position).getQuestionResult().getResultList();
                if (list != null && list.size() > 0) {
                    currentAnswer = list.get(0);
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                ASRResult currentResult = itemList.get(position).getQuestionResult();

                StringBuilder resultTextBuilder = new StringBuilder();
                int i = 0;
                for (String newLine : currentResult.getResultList()) {
                    resultTextBuilder.append(newLine).append(" - ").append(currentResult.getConfidenceArray()[i++]);
                    resultTextBuilder.append("\n");
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(AssistActivity.this);
                builder.setMessage(resultTextBuilder.toString()).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

                return false;
            }
        });

        ASRHandler handler = new ASRHandler() {
            @Override
            public void onEndOfSpeech() {
                listening = false;
            }

            @Override
            public void onError(String error) {
                Snackbar.make(fab, error, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }

            @Override
            public void onResult(final ASRResult result, final Boolean play) {
                listening = false;
                currentAnswer = null;

                if (result != null) {
                    ArrayList<String> list = result.getResultList();
                    if (list != null && list.size() > 0) {
                        final String question = list.get(0);

                        new AnswerTask(new AnswerHandler() {
                            @Override
                            public void onAnswer(String answer) {
                                AssistItem newItem = new AssistItem();
                                newItem.setQuestionResult(result);
                                newItem.setQuestion(question);

                                Boolean hasLink = false;
                                String singleAnswer = "";

                                if (yesNoMode) {
                                    if ("yes".equals(question)) {
                                        hasLink = true;
                                        singleAnswer = currentAnswerList.get(currentAnswerIndex).get(1);
                                        currentAnswerList = null;
                                        currentAnswerIndex = null;
                                        yesNoMode = false;
                                    } else if ("no".equals(question)) {
                                        currentAnswerIndex++;
                                        if (currentAnswerIndex < currentAnswerList.size()) {
                                            singleAnswer = currentAnswerList.get(currentAnswerIndex).get(0);
                                        } else {
                                            currentAnswerList = null;
                                            currentAnswerIndex = null;
                                            yesNoMode = false;
                                            singleAnswer = "May the force be with you!";
                                        }
                                    } else {
                                        singleAnswer = "Could you repeate that?";
                                    }
                                } else {
                                    currentAnswerList = new Gson().fromJson(answer, new TypeToken<ArrayList<ArrayList<String>>>() {
                                    }.getType());

                                    if (currentAnswerList != null && currentAnswerList.size() > 0 && currentAnswerList.get(0).size() > 0) {
                                        currentAnswerIndex = 0;
                                        singleAnswer = currentAnswerList.get(currentAnswerIndex).get(0);
                                        yesNoMode = true;
                                    }

                                }

                                newItem.setAnswer(singleAnswer);
                                currentAnswer = singleAnswer;

                                itemList.add(newItem);
                                adapter.notifyDataSetChanged();

                                if (hasLink) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(currentAnswer));
                                    startActivity(i);
                                } else if (currentAnswer != null && play) {
                                    talker.talk(currentAnswer);
                                }
                            }
                        }, "http://192.168.0.142:8080/ITKAssistRest/api/question", question).execute();
                    }
                }

                partialQuestion.setText("");
                partialQuestion.setVisibility(View.GONE);
            }

            @Override
            public void onPartialResult(ASRResult result) {
                partialQuestion.setVisibility(View.VISIBLE);

                ArrayList<String> list = result.getResultList();
                if (list != null && list.size() > 0) {
                    String question = list.get(0);
                    partialQuestion.setText(question);
                }
            }
        };

        talker = new T2STalker(this, handler);
        talker.setVoice("usenglishfemale");
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(new RecognitionListenerImpl(handler));
    }

    public void changeLanguage(View view) {
        Button button = (Button) view;
        String speechLanguage;
        String lang = (String) button.getText();
        if ("HU".equals(lang)) {
            speechLanguage = "huhungarianfemale";
            lang = "EN";
        } else {
            speechLanguage = "usenglishfemale";
            lang = "HU";
        }
        button.setText(lang);
        talker.setVoice(speechLanguage);
    }

    public void speek(View view) {
        if (currentAnswer != null) {
            talker.talk(currentAnswer);
        }
    }
}