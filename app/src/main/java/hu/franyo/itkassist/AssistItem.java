package hu.franyo.itkassist;

import hu.franyo.itkassist.question.ASRResult;

public class AssistItem {

    private ASRResult questionResult;
    private String question;
    private String answer;

    public ASRResult getQuestionResult() {
        return questionResult;
    }

    public void setQuestionResult(ASRResult questionResult) {
        this.questionResult = questionResult;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
